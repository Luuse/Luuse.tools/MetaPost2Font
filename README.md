# Metapost To Font
by [Luuse](http://luuse.io) 2017

## Requirements
  
  * python
  * fontforge python ([doc](http://fontforge.github.io/python.html))
  * inkscape

## How to use this

You must call yours `.mp` files with dec value. For exemple `A` -> `65.mp`.
Check dec value -> [unicode-table](https://unicode-table.com)

### Metapost files to Svg files

In your directory run `$ bash mp2svg.sh`.

### Svg files StrokeToPath

Run `StrokeToPath.sh`.

### Svg files StrokeToPath

Run `Svg2Font.py`.

## License

[GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html)
