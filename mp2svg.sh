#!/usr/bin/env bash

for LETTER_MP in mp/*.mp; do
  dec=`basename $LETTER_MP .mp`
  mpost -interaction=batchmode -s 'outputformat="svg"' $LETTER_MP
  mv $dec.svg svg-stroke/$dec.svg
  echo $LETTER_MP to svg-stroke/$dec.svg
done

rm *.log
